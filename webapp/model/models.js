sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function (JSONModel, Device) {
	"use strict";

	return {
		
		createDeviceModel: function () {
			var oModel = new JSONModel({
				isTouch: Device.support.touch,
				isNoTouch: !Device.support.touch,
				isPhone: Device.system.phone,
				isTablet: Device.system.tablet,
				isNoTablet: !Device.system.tablet,
				isNoPhone: !Device.system.phone,
				listMode: Device.system.phone ? "None" : "SingleSelectMaster",
				listItemType: Device.system.phone ? "Active" : "Inactive"
			});
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		}

	};
});