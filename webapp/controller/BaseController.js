sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";

	return Controller.extend("hit.sap.fiori.CriarOrdemManut.controller.BaseController", {

		oGlobalBusyDialog: null,
		stackLoading: [],

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		showLoading: function () {
			if (this.stackLoading === undefined || this.stackLoading.length === 0) {
				this.oGlobalBusyDialog = new sap.m.BusyDialog();
				this.oGlobalBusyDialog.open();
			}
			this.stackLoading.push(1);
		},

		closeLoading: function () {
			this.stackLoading.pop(1);
			if (this.stackLoading === undefined || this.stackLoading.length === 0) {
				this.oGlobalBusyDialog.close();
			}
		},

		setFieldError: function (sId, type, text) {
			let oField = this.byId(sId);

			if (type === this.constants.error) {
				oField.setValueState(sap.ui.core.ValueState.Error);
				oField.setValueStateText(text);
				return true;
			} else {
				oField.setValueState(sap.ui.core.ValueState.None);
				return false;
			}
		},

		trataError: function (oError) {
			let sError = this.getResourceBundle().getText("msgError");
			let sFalhaRede = this.getResourceBundle().getText("msgFalhaRede");
			let sNoInternet = this.getResourceBundle().getText("msgNoInternet");
			let errorObj;
			try {
				errorObj = JSON.parse(oError.responseText);
			} catch (err) {

				errorObj = {
					error: {
						message: {
							value: sFalhaRede
						}
					}
				};

				if (oError.responseText === "") {
					errorObj.error.message.value += sNoInternet;
				} else {
					errorObj.error.message.value += oError.responseText.replace(/[\n\r]+/g, " ");
				}
			}
			MessageBox.error(sError + errorObj.error.message.value);
		}
	});
});