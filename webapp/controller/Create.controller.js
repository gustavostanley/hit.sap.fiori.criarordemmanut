sap.ui.define([
	"hit/sap/fiori/CriarOrdemManut/controller/BaseController",
	"hit/sap/fiori/CriarOrdemManut/controller/valueHelp/VHLocal",
	"hit/sap/fiori/CriarOrdemManut/controller/valueHelp/VHEquip",
	"hit/sap/fiori/CriarOrdemManut/model/BarcodeScanner",
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function (BaseController, VHLocal, VHEquip, BarcodeScanner, MessageToast, MessageBox) {
	"use strict";

	return BaseController.extend("hit.sap.fiori.CriarOrdemManut.controller.Create", {

		stackIsReadingQR: [],

		valueHelpLocal: VHLocal,
		valueHelpEquip: VHEquip,

		constants: {
			pathValueHelpLocal: "hit.sap.fiori.CriarOrdemManut.view.valueHelp.VHLocal",
			idValueHelpLocal: "idSelectDialogVHLocal",
			pathValueHelpEquip: "hit.sap.fiori.CriarOrdemManut.view.valueHelp.VHEquip",
			idValueHelpEquip: "idSelectDialogVHEquip",
			idLocalInst: "idLocalInst",
			idDescLocalInst: "idDescLocalInst",
			idEquipamento: "idEquipamento",
			idDescEquipamento: "idDescEquipamento",
			idAnomalia: "idAnomalia",
			idCriticidade: "idCriticidade",
			idContacto: "idContactoProd",
			idDescAnomalia: "idDescAnomalia",
			outras: "OUTR",
			error: "E"
		},

		onInit: function () {

			this._OverwriteLaunchPadBackButton();

			this._OverwriteMobileBackButton();

			let oData = {
				Local: "",
				DescLocal: "",
				Equipamento: "",
				DescEquipamento: "",
				Anomalia: "",
				DescAnomalia: "",
				Criticidade: "",
				Contacto: "",
				showCameraIcon: false
			};

			//Criação de model para manipulação da view
			let oModel = new sap.ui.model.json.JSONModel(oData);
			this.setModel(oModel, "viewModel");

			this._oRouter = this.getOwnerComponent().getRouter();
			this._oRouter.attachRouteMatched(this.onRouteMatched, this);
		},

		onRouteMatched: function () {
			//Regra para exibição da camera
			if (BarcodeScanner.isAvailable()) {
				let oViewModel = this.getModel("viewModel");
				if (oViewModel !== undefined) {
					let oData = oViewModel.getData();
					oData.showCameraIcon = true;
					oViewModel.setData(oData);
				}
			}
		},

		lockBackButton: function () {
			this.stackIsReadingQR.push(1);
		},

		unLockBackButton: function () {
			setTimeout(() => {
				this.stackIsReadingQR.pop(1);
			}, 500);
		},

		_OverwriteLaunchPadBackButton: function () {

			if (sap.ui.getCore().byId("backBtn") === undefined) {
				return;
			}

			//Backup da função original do Back Button do Launchpad
			this.stdBackButton = sap.ui.getCore().byId("backBtn").mEventRegistry.press[0].fFunction;

			//Custom Function
			this.customBackButton = () => {
				if (this.oGlobalBusyDialog !== null) {
					this.oGlobalBusyDialog.close();
				}
				this.stdBackButton();
			};

			//Sobrescreve o Back Button do Launchpad com a função customizada
			sap.ui.getCore().byId("backBtn").mEventRegistry.press[0].fFunction = this.customBackButton;
		},

		_OverwriteMobileBackButton: function () {

			this.onBackButtonWithContext = this.onBackKeyDown.bind(this);

			document.addEventListener("backbutton", this.onBackButtonWithContext, true);
		},

		onBackKeyDown: function () {
			if (this.stackIsReadingQR.length !== undefined && this.stackIsReadingQR.length !== 0) {
				return;
			}
			this.customBackButton();
		},

		onConfirmar: function () {
			this.showLoading();
			//Verifica se todos os campos obrigatórios estão preenchidos e sem erro no value state
			if (!this.validateRequiredFields() || !this.validateValueState()) {
				this.closeLoading();
				return;
			}
			this._executaCriacao();
		},

		_executaCriacao: function () {
			let oViewModel = this.getModel("viewModel");
			let oData = oViewModel.getData();

			let oModel = this.getOwnerComponent().getModel();

			let oNewEntity = {
				I_TPLNR: oData.Local,
				I_EQUNR: oData.Equipamento,
				I_CODING: oData.Anomalia,
				I_DESCCODING: oData.DescAnomalia,
				I_CRIT: oData.Criticidade,
				I_CONTACT: oData.Contacto === "S" ? "X" : ""
			};

			oModel.create("/ORDEMSet", oNewEntity, {
				success: oCreatedEntry => {
					let sText;

					this.closeLoading();
					if (oCreatedEntry.E_NUMORD !== "") {
						sText = this.getResourceBundle().getText("msgSucessoNotaOrdem", [oCreatedEntry.E_NUMNOTA, oCreatedEntry.E_NUMORD]);
					} else {
						sText = this.getResourceBundle().getText("msgSucessoNota", [oCreatedEntry.E_NUMNOTA, oCreatedEntry.E_NUMORD]);
					}
					this.clearAllFields();
					MessageBox.success(sText);
				},
				error: oError => {
					this.trataError(oError);
					this.closeLoading();
				}
			});
		},

		onCancelar: function () {
			if (sap.ui.getCore().byId("backBtn") === undefined) {
				window.history.go(-1);
				return;
			}
			this.customBackButton();
		},

		validateValueState: function () {
			let bSucesso = true;

			if (this.byId(this.constants.idLocalInst).getValueState() !== "None") {
				bSucesso = false;
			}

			if (this.byId(this.constants.idEquipamento).getValueState() !== "None") {
				bSucesso = false;
			}
			return bSucesso;
		},

		validateRequiredFields: function () {
			let bSucesso = true;
			let sRequiredText = this.getResourceBundle().getText("requiredText");

			//Local de Instalação			
			if (this.setFieldError(this.constants.idLocalInst, (this.byId(this.constants.idLocalInst).getValue() === "" ? this.constants.error :
					""), sRequiredText)) {
				bSucesso = false;
			}

			//Anomalia
			if (this.setFieldError(this.constants.idAnomalia, (this.byId(this.constants.idAnomalia).getSelectedKey() === "" ? this.constants.error :
					""), sRequiredText)) {
				bSucesso = false;
			}

			//Descr Anomalia
			if (this.byId(this.constants.idAnomalia).getSelectedKey() === this.constants.outras && this.byId(this.constants.idDescAnomalia).getValue() ===
				"") {
				this.setFieldError(this.constants.idDescAnomalia, this.constants.error, sRequiredText);
				bSucesso = false;
			} else {
				this.setFieldError(this.constants.idDescAnomalia, "", sRequiredText);
			}

			//Criticidade
			if (this.setFieldError(this.constants.idCriticidade, (this.byId(this.constants.idCriticidade).getSelectedKey() === "" ? this.constants
					.error :
					""), sRequiredText)) {
				bSucesso = false;
			}

			//Contacto Produto
			if (this.setFieldError(this.constants.idContacto, (this.byId(this.constants.idContacto).getSelectedKey() === "" ? this.constants
					.error :
					""), sRequiredText)) {
				bSucesso = false;
			}

			return bSucesso;
		},

		onExit: function () {
			document.removeEventListener("backbutton", this.onBackButtonWithContext, true);
			this._RetornaBackButtonStd();
		},

		_RetornaBackButtonStd: function () {

			if (sap.ui.getCore().byId("backBtn") === undefined) {
				return;
			}

			//Retorna a função original para o Back Button do Launchpad
			sap.ui.getCore().byId("backBtn").mEventRegistry.press[0].fFunction = this.stdBackButton;
		},

		onChangeLocal: function () {
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			this.showLoading();
			this.getValidadorLocal().then(oData => {

				if (oData.LocalID) {
					oViewData.DescLocal = oData.DescricaoLocal;
				} else {
					oViewData.DescLocal = "";
				}

				oViewData.Equipamento = "";
				oViewData.DescEquipamento = "";

				oViewModel.setData(oViewData);
				this.closeLoading();
			}).catch(() => {
				oViewData.DescLocal = "";
				oViewData.Equipamento = "";
				oViewData.DescEquipamento = "";
				oViewModel.setData(oViewData);
				this.closeLoading();
			});
		},

		onChangeEquipamento: function () {
			let oViewModel = this.getModel("viewModel");
			let oViewData = oViewModel.getData();

			this.showLoading();
			this.getValidadorEquipamento().then(oData => {

				if (oData.EquipamentoID) {
					oViewData.DescEquipamento = oData.DescricaoEquipamento;
				} else {
					oViewData.DescEquipamento = "";
				}

				if (oData.LocalID) {
					this.setFieldError(this.constants.idLocalInst, "");
					oViewData.Local = oData.LocalID;
					oViewData.DescLocal = oData.to_LocalInstalacao.DescricaoLocal;
				} else {
					oViewData.Local = "";
					oViewData.DescLocal = "";
				}

				oViewModel.setData(oViewData);
				this.closeLoading();
			}).catch(() => {
				oViewData.oViewData = "";
				oViewModel.setData(oViewData);
				this.closeLoading();
			});
		},

		getValidadorLocal: function () {
			let sValue = this.byId(this.constants.idLocalInst).getValue().trim();
			let oModel = this.getOwnerComponent().getModel("LocalModel");

			return new Promise((resolve, reject) => {
				//Não existe valor a ser validado
				if (sValue === "") {
					this.setFieldError(this.constants.idLocalInst, "");
					resolve("NO_VALUE");
					return;
				}
				//Serviço para validação dos valores
				let sPath = `/ZCDS_GET_FUNC_LOCATION('${ sValue }')`;
				//Executa leitura do Local
				oModel.read(sPath, {
					success: oData => {
						this.setFieldError(this.constants.idLocalInst, "");
						resolve(oData);
					},
					error: oError => {
						if (oError.statusCode == "404") {
							let sErrorLocal = this.getResourceBundle().getText("msgLocalInvalido");
							this.setFieldError(this.constants.idLocalInst, this.constants.error, sErrorLocal);
						} else {
							this.trataError(oError);
						}
						reject(oError);
					}
				});
			});
		},

		getValidadorEquipamento: function () {
			let sValue = this.byId(this.constants.idEquipamento).getValue().trim();
			let oModel = this.getOwnerComponent().getModel("EquipamentoModel");

			return new Promise((resolve, reject) => {
				//Não existe valor a ser validado
				if (sValue === "") {
					this.setFieldError(this.constants.idEquipamento, "");
					resolve("NO_VALUE");
					return;
				}

				let sPath = `/ZCDS_EQUIPAMENTO('${ sValue }')`;
				//Executa leitura do Equipamento
				oModel.read(sPath, {
					urlParameters: {
						"$expand": "to_LocalInstalacao"
					},
					success: oData => {
						this.setFieldError(this.constants.idEquipamento, "");
						resolve(oData);
					},
					error: oError => {
						if (oError.statusCode == "404") {
							let sErrorEquipamento = this.getResourceBundle().getText("msgEquipamentoInvalido");
							this.setFieldError(this.constants.idEquipamento, this.constants.error, sErrorEquipamento);
						} else {
							this.trataError(oError);
						}
						reject(oError);
					}
				});
			});
		},

		onReadLocal: function () {
			this.lockBackButton();

			this.getReader().then(sLocal => {
				this.byId(this.constants.idLocalInst).setValue(sLocal);
				this.onChangeLocal();
				this.unLockBackButton();
			}).catch(oError => {
				this.unLockBackButton();
				if (oError.error === "NOT_SUPPORTED") {
					let sErrorMessage = this.getResourceBundle().getText("msgBarcodeNotPossible");
					MessageBox.error(sErrorMessage);
				}
			});
		},

		onReadEquipamento: function () {
			this.lockBackButton();

			this.getReader().then(sEquip => {
				this.byId(this.constants.idEquipamento).setValue(sEquip);
				this.onChangeEquipamento();
				this.unLockBackButton();
			}).catch(oError => {
				this.unLockBackButton();
				if (oError.error === "NOT_SUPPORTED") {
					let sErrorMessage = this.getResourceBundle().getText("msgBarcodeNotPossible");
					MessageBox.error(sErrorMessage);
				}
			});
		},

		getReader: function () {
			return new Promise((resolve, reject) => {
				BarcodeScanner.scan(
					mResult => {
						if (!mResult.cancelled) {
							resolve(mResult.text);
						} else {
							reject(mResult);
						}
					},
					mError => {
						reject(mError);
					}
				);
			});
		},

		clearAllFields: function () {
			let oViewModel = this.getModel("viewModel");
			let oData = oViewModel.getData();

			oData.Local = "";
			oData.DescLocal = "";
			oData.Equipamento = "";
			oData.DescEquipamento = "";
			oData.Anomalia = "";
			oData.DescAnomalia = "";
			oData.Criticidade = "";
			oData.Contacto = "";

			oViewModel.setData(oData);
		}
	});
});