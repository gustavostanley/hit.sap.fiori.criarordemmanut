sap.ui.define([
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Filter, FilterOperator) {
	"use strict";

	return {

		handleVHLocal: function () {
			if (!this._oValueHelpLocal) {
				this._oValueHelpLocal = sap.ui.xmlfragment(this.constants.pathValueHelpLocal, this);
				this.getView().addDependent(this._oValueHelpLocal);
			}
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpLocal);
			oSelectDialog.getBinding("items").filter([]);
			this._oValueHelpLocal.open();
		},

		handleVHLocalSearch: function (oEvent) {
			let aFilter;
			let sQueryLocal = oEvent.getSource()._searchField.getValue();
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpLocal);

			if (sQueryLocal) {
				aFilter = new Filter({
					filters: [
						new Filter("LocalID", FilterOperator.Contains, sQueryLocal),
						new Filter("DescricaoLocal", FilterOperator.Contains, sQueryLocal)
					],
					or: true
				});
			} else {
				aFilter = [];
			}

			oSelectDialog.getBinding("items").filter(aFilter);
		},

		handleVHLocalSelect: function (oEvent) {
			let oPopupModel = this.getModel("viewModel");
			let oModelData = oPopupModel.getData();
			let oSelectedItem = oEvent.getSource()._oList.getSelectedItem();
			
			oModelData.Local = oSelectedItem.getProperty("title");
			oModelData.DescLocal = oSelectedItem.getProperty("description");
			oModelData.Equipamento = "";
			oModelData.DescEquipamento = "";
			oPopupModel.setData(oModelData);
			this.setFieldError(this.constants.idLocalInst, "");
		}

	};
});