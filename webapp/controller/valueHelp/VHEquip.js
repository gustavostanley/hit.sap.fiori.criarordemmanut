sap.ui.define([
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Filter, FilterOperator) {
	"use strict";

	return {

		handleVHEquip: function () {
			if (!this._oValueHelpEquip) {
				this._oValueHelpEquip = sap.ui.xmlfragment(this.constants.pathValueHelpEquip, this);
				this.getView().addDependent(this._oValueHelpEquip);
			}
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpEquip);
			let aFilter = this.valueHelpEquip.getVHBaseFilter.bind(this)();
			oSelectDialog.getBinding("items").filter(aFilter);
			this._oValueHelpEquip.open();
		},

		handleVHEquipSearch: function (oEvent) {
			let aFilter = [];
			let aFilterBase = this.valueHelpEquip.getVHBaseFilter.bind(this)();
			let sQueryEquip = oEvent.getSource()._searchField.getValue();
			let oSelectDialog = sap.ui.getCore().byId(this.constants.idValueHelpEquip);

			if (sQueryEquip) {
				//Filtros para campo de pesquisa
				let searchFilter = new Filter({
					filters: [
						new Filter("EquipamentoID", FilterOperator.Contains, sQueryEquip),
						new Filter("DescricaoEquipamento", FilterOperator.Contains, sQueryEquip)
					],
					or: true
				});

				if (aFilterBase.length) {
					let aConcat = [searchFilter];
					aFilterBase.forEach(curr => aConcat.push(curr));
					aFilter = new Filter({
						filters: aConcat,
						and: true
					});
				} else {
					aFilter = searchFilter;
				}

			} else {
				aFilterBase.forEach(curr => aFilter.push(curr));
			}

			oSelectDialog.getBinding("items").filter(aFilter);
		},

		getVHBaseFilter: function () {
			let aFilter = [];
			let sLocal = this.getModel("viewModel").getData().Local;
			if (sLocal) {
				aFilter.push(new Filter("LocalID", FilterOperator.EQ, sLocal));
			}
			return aFilter;
		},

		handleVHEquipSelect: function (oEvent) {
			let oSelectedItem = oEvent.getSource()._oList.getSelectedItem();
			this.byId(this.constants.idEquipamento).setValue(oSelectedItem.getProperty("title"));
			this.onChangeEquipamento();
		}

	};
});