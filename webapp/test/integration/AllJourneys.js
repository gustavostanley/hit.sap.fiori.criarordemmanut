/* global QUnit*/

sap.ui.define([
	"sap/ui/test/Opa5",
	"hit/sap/fiori/CriarOrdemManut/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"hit/sap/fiori/CriarOrdemManut/test/integration/pages/Create",
	"hit/sap/fiori/CriarOrdemManut/test/integration/navigationJourney"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "hit.sap.fiori.CriarOrdemManut.view.",
		autoWait: true
	});
});